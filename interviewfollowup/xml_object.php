<?php
//////////////////////////////////////////////////////////////////////////////
// Class: Xml_Object
//
// This class is an attempt to save a large amount of work on fairly standard
// objects.  
//
// @author     Original Author ben.roscoe@plexus.com
// @copyright  2006 Plexus.


    abstract class Xml_Object extends DB_Collection{
        
        protected $_ID;
        protected $_Old_ID;
        protected $_Updated;
        protected $_Status;
        protected $_Value;
        
        protected $_Instruction = '';
        protected $_Input_Label;
        protected $_Type = 'standard';
        protected $_Name = '';
        protected $_Length = '';
        protected $_Read_Only = '';
        protected $_Phone = '';
        protected $_Cal = '';
        
        //this is an array of item names to be included when converting to xml
        protected $_DB_Data = Array('_ID', '_Value');
        protected $_Object_Data = Array('_Instruction', '_Input_Label', '_Type', '_Name',
                                        '_Length', '_Read_Only', '_Phone', '_Cal');
        
        public function __construct(){
            Log::write(__METHOD__.':',LOG_LEVEL_DEBUG_2);
            //assumption made that DB table name matches class name!!!!!!!
            $name = preg_replace('/_/','',get_class($this));
            $this->_table_info['name']=$name;
            $this->_Name = preg_replace('/s$/', '', $name);
            parent::__construct();
            //echo '<pre>'.__METHOD__; print_r($this); echo '</pre>';
        }
        
        public function getID(){ return $this->_ID; }
        public function getOldID(){ return $this->_Old_ID; }
        public function getUpdated(){ return $this->_Updated; }
        public function getStatus(){ return $this->_Status;}
        public function getInstruction(){ return $this->_Instruction;}
        public function getValue(){ return $this->_Value;}
        public function getInputLabel(){ return $this->_Input_Label;}
        public function getName(){ return $this->_Name;}
        
        public function setID( $val ){ $this->_ID = $val; }
        public function setOldID( $val ){ $this->_Old_ID = $val; }
        public function setUpdated( $val ){ $this->_Updated = $val; }
        public function setStatus($val){ $this->_Status = $val;}
        public function setValue( $val ){ $this->_Value = $val; }
        public function setInputLabel( $val ){ $this->_Input_Label = $val; }
        public function setName( $val ){ $this->_Name = $val; }
        
        public function loadFromPost( ){
            $type = '';
            $nameNoEndS =$this->_Name;
            $toreturn = false;
            if( $this->_Type == 'standard' ){
                $type = 'Input';
            }else if( $this->_Type == 'select' ){
                $type = 'Select';
            }
            if( isset($_POST[$type.$nameNoEndS]) ){
                if( $this->_Type == 'select' ){
                    $this->setStatus('active');
                    $this->retrieveCollectionByValues();
                    $this->moveItemToFront( $_POST[$type.$nameNoEndS] );//print$_POST[$nameNoEndS."ID"];
                    $this->_Old_ID = $_POST[$nameNoEndS."ID"];
                }else{
                    $this->_Value = $_POST[$type.$nameNoEndS];
                    $this->_ID = $_POST[$nameNoEndS."ID"];
                }
                $toreturn = true;
            }else{
                $this->_Value='';
                $this->_ID = 0;
            }
            return $toreturn;
        }
        
        public function validatePostData(&$warnings, &$errors){
            return true;
        }
        
        public function addSearchSql( &$from, &$where){
            if( $this->_Value != '' && $this->_Type != 'select'){
		$from[] = "{$this->_Name}s on P.{$this->_Name}ID = {$this->_Name}s.ID";
		$where[] = "{$this->_Name}s.Value like '%{$this->_Value}%' and {$this->_Name}s.Status = 'active'";
            }else if( $this->_Type == 'select' && $this->_ID != "1" ){
		$from[] = "{$this->_Name}s on P.{$this->_Name}ID = {$this->_Name}s.ID";
		$where[] = "{$this->_Name}s.ID like '%{$this->_ID}%' and {$this->_Name}s.Status = 'active'";
            }
        }
        
        public function loadValidateSearch( &$warnings, &$errors, &$from, &$where ){
            $this->loadFromPost();
            $toreturn = $this->validatePostData( $warnings, $errors);
            $this->addSearchSql( $from, $where);
            return $toreturn;
        }
        
        // Function: insertWithChangeLog
        // Description: creates a change log for an object
        // Inputs: Purch Req ID,
        // Outputs: status
        public function insertWithChangeLog($prID){
            global $cfg;
            $thisclass = get_class($this);
            $oldobj;
            $change = new Change_Logs();
            $change->setRelatedTable($thisclass);
            $change->setPurchaseRequestID($prID);
            
            if( $this->_Type != 'select' ){
                $change->setOldRelatedTableID($this->_ID);
                eval('$oldobj = new '.$thisclass.'();');
                $oldobj->setID($this->_ID);
                if( !$oldobj->retrieveByPrimaryKey() ){
                    Log::write(__METHOD__.': Error finding item to replace. Key: '.$thisclass.' ID: '.$oldobj->getID(),'LOG_LEVEL_WARN');
                    return false;
                }
                if($this->_Name == 'Line_Item'){
                    $this->_Purchase_Request_ID = $prID;
                }
                $this->_ID = 0;
                if( !$this->insert() ){
                    Log::write(__METHOD__.': Error inserting new item to replace item','LOG_LEVEL_WARN');
                    return false;
                }
            }else{
                $change->setOldRelatedTableID($this->_Old_ID);
            }
            //eval('$this->_'.$key.'_ID = $obj->getID();');
            $change->setNewRelatedTableID($this->_ID);
            $change->setStatus('active');
            $change->setUserID($cfg->user->getID());
            if( !$change->insert() ){
                Log::write(__METHOD__.': Error inserting change log item',LOG_LEVEL_WARN);
                return false;
            }
            if( $this->_Type != 'select' ){
                $oldobj->setStatus("replaced");
                $oldobj->update();
            }
            
            return true;
        }
        
        
        // Function: toXml
        // Description: Takes the data from the object and converts it to xml based
        //              on the standardized naming conventions
        // Inputs: Requires the document you will be inserting this into
        // Outputs: xml node
        public function toXml($doc, $root, $search){
            //Log::write(__METHOD__.': doc='.$doc,LOG_LEVEL_DEBUG_2);
            $name = $this->_Name.'s';
            
            $node = $doc->createElement( "$name" );
            $root->appendChild($node);
            
            foreach($this->_Object_Data as $conv){
                $name = preg_replace('/_/','',$conv);
                eval('$tempval = $this->'.$conv.';');
                if( $tempval != '' ){
                    $tempNode2 = $doc->createElement( $name, $tempval );
                    $node->appendChild($tempNode2);
                }
            }
            if( count($this->_items) > 0){
                $tempNode3 = $doc->createElement( "Items" );//print"hi";
                $node->appendChild($tempNode3);
                //now create a sub node for each item in the list
                foreach ($this->_items as $item) {
                    if( !(!$search && strcmp($this->_Type,'select')==0 && $item['ID'] == 1) 
                       || strcmp($this->_Name,'RequestSensitive')==0 
                       || strcmp($this->_Name,'NonjobAccount')==0 
                       || strcmp($this->_Name,'NonjobGroup')==0){
                        $tempNode = $doc->createElement( "Item" );//print"hi";
                        $tempNode3->appendChild($tempNode);
                        //now fill the sub node with all the values from that node
                        foreach ($item as $key => $value){
                            if( strcmp($this->_Name, 'DueDate')==0 && $key=="Value"){
                                $value = Date::generateDateForWeb($this->_Value);
                            }
                            //$tempNode2 = $doc->createElement($key, $value);
                            //$tempNode->appendChild($tempNode2);
                            $tempNode2 = $doc->createElement($key);
                            $tempNode->appendChild($tempNode2);
                            $tempNode2->appendChild($doc->createCDATASection($value));
                        }
                    }
                }
            }else{
                if( !(!$search && strcmp($this->_Type,'select')==0 && $this->_ID == 1) ){
                    $tempNode = $doc->createElement( "Item" );
                    $node->appendChild($tempNode);
                    //now fill the sub node with all the values from that node
                    foreach( $this->_DB_Data as $conv ){
                        $name = preg_replace('/_/','',$conv);
                        eval('$tempval = $this->'.$conv.';');
                        if( strcmp($this->_Name, 'DueDate')==0 && $name=="Value"){
                            $tempval = Date::generateDateForWeb($this->_Value);
                        }
                        //$tempNode2 = $doc->createElement( $name, $tempval );
                        //$tempNode->appendChild($tempNode2);
                        $tempNode2 = $doc->createElement( $name);
                        $tempNode->appendChild($tempNode2);
                        $tempNode2->appendChild($doc->createCDATASection($tempval));
                    }
                }
            }
            //$this->_Value = $oldval;
            return true;
    }
        
        public function insert(){
            $this->_Updated = Date::sqlStamp();
            return parent::insert();
        }
        
        public function update($doUpdated=true){
            if( $doUpdated ){
                $this->_Updated = Date::sqlStamp();
            }
            return parent::update();
        }
        
        // Function: toCall
        // Description: returns an array with the names of all methods
        // that need to be called to properly display the data in this 
        // object as it pertains to Change_Logs
        // Inputs: 
        // Outputs: array or false
        abstract public function toCall();
        
        // Function: field
        // Description: returns name of field for change logs 
        // Inputs: 
        // Outputs: array or false
        abstract public function field();
        
        protected function myEmpty( $var ){
            if( empty($var) && !is_numeric($var) ){
                return true;
            }else{
                return false;
            }
        }
    }
?>
