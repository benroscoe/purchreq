<?php
    include('../init.php');

    //create website xml document
    $website = new DOM_UTF('1.0', 'ISO-8859-1');

    $action = 'nothing';
    if(array_key_exists('action', $_POST)) {
        $action = $_POST['action'];
    }else if(sizeof($_GET) > 0) {
        $tempaction = array_keys($_GET);
        $action = array_shift($tempaction);
        
        if($action == 'action'){
            $action = $_GET[$action];
        }
    }


    //Determine which stylesheet to use
    switch( $action ){
        case 'login':
            $stylesheet = 'styles/login.xslt';
            $website->appendChild($website->createProcessingInstruction(
                'xml-stylesheet', 'href="styles/login.xslt" type="text/xsl"'));
            break;
        default:
            $stylesheet = 'styles/index.xslt';
            $website->appendChild($website->createProcessingInstruction(
                'xml-stylesheet', 'href="styles/index.xslt" type="text/xsl"'));
            break;
    }

    $root = $website->createElement( "PurchaseWebsite" );
    $root->setAttributeNode(new DOMAttr('id','WebRoot'));
    $website->appendChild( $root );
    
    $mess = new Messages($website, $website->documentElement);
    
    if(!in_array($action, array('login'))) {
	        Auth::authRequireLogin('index.php?action=login');
    }
    
    //Send message to user if illegal action was taken
    if( isset($_GET['illegal']) ){
        Messages::addMessage("You have attempted to perform an ILLEGAL action.  By default the PR you were working on is reloaded and any changes you may have been attempting to save have been lost.  This commonly happens when using the back button to attempt to go back and edit a purchase request that has been saved.");
        
    }
    
    //send to controllers main window
    $maincontrol = new Main_Window_Controller($action);
    switch( $action ){
        case 'login':
            Login::attemptLogin("index.php");
            break;
        case 'logout':
            session_destroy();
            Url::urlRedirect('index.php');
            break;
        case 'Save Role':
            $maincontrol->SaveRole($website, $website->documentElement);
            Messages::addMessage("Welcome to the Purchase Request System.");
            break;
        case 'newpr':
             Log::write(__METHOD__.': "newpr"' ,LOG_LEVEL_INFO);
            $website->documentElement->appendChild($website->createElement("CheckStatus", 'unchecked'));
            if( $maincontrol->makeNewPurchaseRequest($website, $website->documentElement) ){
                Messages::addMessage("This Purchase Request was filled in with data from your last Purchase Request.  Please update the information as needed.");
            } else {
                Log::write(__METHOD__.': failed to correctly retrieve purchase request' ,LOG_LEVEL_WARN);
            }
            break;
        case 'Duplicate Request':
             Log::write(__METHOD__.': "Duplicate Request"' ,LOG_LEVEL_INFO);
            $website->documentElement->appendChild($website->createElement("CheckStatus", 'unchecked'));
            if( $maincontrol->makeNewPurchaseRequest($website, $website->documentElement, false, true) ){
                Messages::addMessage("Purchase Request was copied from a previous request.  Please edit fields as needed.");
            } else {
                Log::write(__METHOD__.': failed to correctly retrieve purchase request' ,LOG_LEVEL_WARN);
            }
            break;
        case 'newblankpr':
             Log::write(__METHOD__.': "newblankpr"' ,LOG_LEVEL_INFO);
            $website->documentElement->appendChild($website->createElement("CheckStatus", 'unchecked'));
            if( $maincontrol->makeNewPurchaseRequest($website, $website->documentElement, true) ){
                Messages::addMessage("This Purchase Request was filled in with default data");
            } else {
                Log::write(__METHOD__.': failed to correctly retrieve purchase request' ,LOG_LEVEL_WARN);
            }
            break;
        case 'clonesearch':
             Log::write(__METHOD__.': "clonesearch"' ,LOG_LEVEL_INFO);
            $return = $maincontrol->CloneSearch($website, $website->documentElement);
            break;
        case 'Save to Draft':
            Log::write(__METHOD__.': "Save to Draft"' ,LOG_LEVEL_INFO);
            $maincontrol->Save($website, $website->documentElement,"draft","draft","Request Saved");
            break;
        case 'Save':
             Log::write(__METHOD__.': "Save"' ,LOG_LEVEL_INFO);
            $maincontrol->Save($website, $website->documentElement,true,true,"Request Saved");
            break;
        case 'Submit Anyway':
            Log::write(__METHOD__.': "Submit Anyway"' ,LOG_LEVEL_INFO);
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "submitted", false, false, false);
            $maincontrol->mailSubmit();           
            break;
        case 'Re-Submit Anyway':
            Log::write(__METHOD__.': "Re-Submit Anyway"' ,LOG_LEVEL_INFO);
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "onholdsubmitted", false, false, false); 
            $maincontrol->mailSubmit();           
            break;
        case 'Approve Anyway':
             Log::write(__METHOD__.': "Approve Anyway"' ,LOG_LEVEL_INFO);
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "approved", false, false, false);  
            $maincontrol->mailApprove();          
            break;
        case 'Save and Notify':
             Log::write(__METHOD__.': "Save and Notify"' ,LOG_LEVEL_INFO);
            $maincontrol->Save($website, $website->documentElement,true,true,"Request Saved");
            $maincontrol->mailNotify();
            break;
        case 'Revert to Draft': //demotes a submitted PR down to a draft
             Log::write(__METHOD__.': "Revert to Draft"' ,LOG_LEVEL_INFO);
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "draft", false, true);
            $maincontrol->mailDraft();
            break;
        case 'Revert to On-Hold Draft':
             Log::write(__METHOD__.': "Revert to On-Hold Draft"' ,LOG_LEVEL_INFO);
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "onhold", false, true);
            $maincontrol->mailDraft();
            break;
        case 'Revert to Submitted':
             Log::write(__METHOD__.': "Revert to Submitted"' ,LOG_LEVEL_INFO); //APPROVE?
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "submitted", false, true);
            $maincontrol->mailSubmitted();
            break;
        case 'Revert to On-Hold Submitted':
            Log::write(__METHOD__.': "Revert to On-Hold Submitted"' ,LOG_LEVEL_INFO);
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "onholdsubmitted", false, true);
            $maincontrol->mailSubmitted();
            break;
        case 'Ordered':
             Log::write(__METHOD__.': "Ordered"' ,LOG_LEVEL_INFO);
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "ordered", false, false, false);
            $maincontrol->mailOrdered();
            break;
        case 'Place On-hold':
             Log::write(__METHOD__.': "Place On-hold"' ,LOG_LEVEL_INFO);
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "onhold", false, true, false);
            $maincontrol->mailOnhold();
            break;
        case 'Archive Request':
        case 'Cancel Request':
             Log::write(__METHOD__.': "Cancel Request"' ,LOG_LEVEL_INFO);
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "closed",false,false,false);
            $maincontrol->mailKill();
            break;
        case 'Fix Problems':
             Log::write(__METHOD__.': "Fix Problems"' ,LOG_LEVEL_INFO);
            $maincontrol->CheckForErrors($website, $website->documentElement, true); 
            break;    
        case 'Submit Now':
             Log::write(__METHOD__.': "Submit Now"' ,LOG_LEVEL_INFO);
            $return = $maincontrol->SubmitNow($website, $website->documentElement); //APPROVE
            if($return){$maincontrol->mailSubmit();}
            break;
        case 'Submit':
             Log::write(__METHOD__.': "Submit"' ,LOG_LEVEL_INFO);
            $return = $maincontrol->Save($website, $website->documentElement,"submitted","submitted" 
                                         ,"Request submitted and managers have been notified"
                                         ,"Errors were found in the request.  Please click the Fix Problems to fix these errors.  You cannot submit the request until all errors have been fixed."
                                         ,"Warnings were found in the request.  You must either click Submit Anyway, if you view the warnings as acceptable, or click Fix Problems if they are not acceptable.", false);
            if($return){$maincontrol->mailSubmit();} //APPROVE
            break;
        case 'Re-Submit':
             Log::write(__METHOD__.': "Re-Submit"' ,LOG_LEVEL_INFO);
            $return = $maincontrol->Save($website, $website->documentElement,"onholdsubmitted","onholdsubmitted"
                                         ,"Request re-submitted and managers have been notified"
                                         ,"Errors were found in the request.  Please click the Fix Problems to fix these errors.  You cannot re-submit the request until all errors have been fixed."
                                         ,"Warnings were found in the request.  You must either click Re-Submit Anyway, if you view the warnings as acceptable, or click Fix Problems if they are not acceptable.", false);
            if($return){$maincontrol->mailSubmit();} //APPROVE
            break;
        case 'Claim':
             Log::write(__METHOD__.': "Claim"' ,LOG_LEVEL_INFO);
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "claimed", true);
            $maincontrol->mailClaim();
            break;
        case 'Revert to Claimed':
             Log::write(__METHOD__.': "Revert to Claimed"' ,LOG_LEVEL_INFO);
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "claimed", true);
            $maincontrol->mailReClaim();
            break;
        case 'Steal Request':
             Log::write(__METHOD__.': "Steal Request"' ,LOG_LEVEL_INFO);
            $maincontrol->SaveEvenWithErrors($website, $website->documentElement, "claimed", true);
            $maincontrol->mailClaim();
            break;
        case 'Save Claimed':
             Log::write(__METHOD__.': "Save Claimed"' ,LOG_LEVEL_INFO);
            $return = $maincontrol->Save($website, $website->documentElement,"claimed","claimed","Request Saved");
            break;
        case 'Approve':
            Log::write(__METHOD__.': "Approve"' ,LOG_LEVEL_INFO);
            $return = $maincontrol->Approve($website, $website->documentElement, false);
            if($return){$maincontrol->mailApprove();} //APPROVE
            break;
        case 'Re-Approve':
             Log::write(__METHOD__.': "Re-Approve"' ,LOG_LEVEL_INFO);
            $maincontrol->Approve($website, $website->documentElement, true);
            $maincontrol->mailReApprove(); //APPROVE
            break;
        case 'prlink' : //when you click on a prlink on your home page
             Log::write(__METHOD__.': "prlink"' ,LOG_LEVEL_INFO);
            if( $maincontrol->buildRequestByID($website, $website->documentElement, $_GET[$action]) ){
                $website->documentElement->appendChild( 
                            $website->createElement("CheckStatus", 'passed'));
            } else {
                Log::write(__METHOD__.': failed to correctly retrieve purchase request' ,LOG_LEVEL_WARN);
            }
            break;
        case 'nothing':
             Log::write(__METHOD__.': "nothing"' ,LOG_LEVEL_INFO);
            break;
        default:
             Log::write(__METHOD__.': "default"' ,LOG_LEVEL_INFO);
            $temp = preg_replace('/\s/','',$action);
            if( method_exists($maincontrol,$temp) ){
                eval('$maincontrol->'.$temp.'($website, $website->documentElement);');
            }
            break;
    }
    
    //side window controller
    switch($action){
        case 'login':
            break;
        default:
            $side = new Side_Window_Controller();
            $side->defaultList($website, $website->documentElement);
            break;
    }
    
        //$xslt = new XSLTProcessor();

        //$xsl = domDocument::load($stylesheet);
        //$xslt->importStylesheet($xsl);

        //print $xslt->transformToXML($website);
        // set charset to display accented characters correctly
        //header("Content-type: text/xml");
    header('content-type: application/xml; charset=UTF-8');

    // disable any caching by the browser
    //header('Expires: Mon, 14 Oct 2002 05:00:00 GMT'); // Date in the past
    //header('Last-Modified: ' .gmdate("D, d M Y H:i:s") .' GMT'); // always modified
    //header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP 1.1
    //header('Cache-Control: post-check=0, pre-check=0', false);
    //header('Pragma: no-cache'); // HTTP 1.0
        //$website->save('test.xml');
        //Log::write($website->saveXML() ,LOG_LEVEL_INFO);
        print $website->saveXML();

 ?>
