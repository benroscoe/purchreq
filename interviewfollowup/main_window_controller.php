<?php
//////////////////////////////////////////////////////////////////////////////
// Class: MainWindowController
//
// This class will determine what is put into the main window
//
// @author     Original Author ben.roscoe@plexus.com
// @copyright  2005 Plexus.


    
    global $cfg;
    class Main_Window_Controller {
        
        private $_newStatus = '';
        private $_sort = 'ASC';
        private $_new = false;
        private $_redisplayLineItems;
        public $request;
        public $validate;
        public $mailer;
        private $_action;
        
        public function __construct($action){
            $this->mailer = new Main_Controller_Mail($this->request);
            $this->_action = $action;
        }
        
        public function makeNewPurchaseRequest($doc, $root, $autoblank=false, $redisplayLineItems=false){
            Log::write(__METHOD__.':',LOG_LEVEL_DEBUG_2);            
            
            global $cfg;
            
            $this->_newStatus = "unsaved";
            $this->_new = true;
            $this->_redisplayLineItems = $redisplayLineItems;
            $this->_sort = 'DESC';
            
            if( $cfg->user !== false ){
                if( $redisplayLineItems==false && !$autoblank && 
                    $this->buildRequestByUser($cfg->user, $doc, $root) ){
                    
                    return true;
                }else if(!$autoblank && isset($_POST['PRID']) && 
                         $this->buildRequestByID($doc, $root, $_POST['PRID'])){
                    return true;           
                }else{
                    $requests = new Purchase_Requests();
                    $requests->makeNewRequestWithInstructions($cfg->user);
                    $requests->setID(0);
                    $requests->setStatus("unsaved");    
                    $requests->setLineItems(new Line_Items());
                    $requests->getLineItems()->addEmptyItems(5);
                    $requests->setRequestorID($cfg->user->getID());
                    $date = gmdate ("Y-m-d", gmmktime (0,0,0,gmdate('m'),gmdate('d')+7,gmdate('Y')));
                    $requests->getDueDate()->setValue($date);
                    $requests->setVersion($requests->getCurrentVersion());
                    $requests->setCreationDate(Date::sqlStamp());
                    $requests->toXml($doc, $root);
                    return true;
                }
            }else{
                Log::write(__METHOD__.': User object is not set, there has been a problem finding the user information' ,LOG_LEVEL_WARN);
            }
            return false;
            
        }
        
        //this function is used to generate a puchase request
        //document based on a user that it is given as input 
        public function buildRequestByUser($user, $doc, $root, $chkstatus='passed'){
          
            $requests = new Purchase_Requests();
            $requests->setRequestorID( $user->getID() );
            if( !$this->_retrieveRequests($doc, $root, $requests, $chkstatus) ){
                Log::write(__METHOD__.': No Requests found by ID',LOG_LEVEL_INFO);
                return false;
            }
            return true;
        }
        
        // Function: buildRequestByID
        // Description: Finds a purchase request by ID for the main window
        // Inputs: Requires the document you will be inserting this into
        // Outputs: status
        public function buildRequestByID($doc, $root, $PRID, $chkstatus='passed'){
          
            $requests = new Purchase_Requests();
            $requests->setID( $PRID );
            if( !$this->_retrieveRequests($doc, $root, $requests, $chkstatus) ){
                Log::write(__METHOD__.': No Requests found by ID: '.$PRID,LOG_LEVEL_INFO);
                return false;
            }
            
            return true;            
        }
        
        // Function: _retrieveRequests
        // Description: Finds a purchase request by the values filled in the request
        // Inputs: Requires the document you will be inserting this into
        // Outputs: status
        // TODO: Add non-job to search
        public function _retrieveRequests($doc, $root, $requests, $chkstatus='passed'){
            global $cfg;
            if( !$requests->retrieveCollectionByValues('ID', $this->_sort) ) {
                Log::write(__METHOD__.': No Requests found',LOG_LEVEL_INFO);
                return false;
            }
            if( !empty($this->_newStatus) ){
                $requests->setStatus($this->_newStatus);
            }
            $requests->fillChildObjects();
            if( $this->_new ){
                $requests->setID(0);
                $requests->getFirstManager()->setID(1);
                $requests->getSecondManager()->setID(1);
                $requests->getThirdManager()->setID(1);
                $requests->getFourthManager()->setID(1);
                $requests->getFifthManager()->setID(1);
                $requests->setBuyer(new Users('Buyer'));
                $requests->setBuyerID(1);
                $requests->getBuyer()->setID(1);
                if(!$this->_redisplayLineItems){
                    $requests->setLineItems(new Line_Items());
                    $requests->getLineItems()->addEmptyItems(5);
                }else{
                    $requests->setRequestorID($cfg->user->getID());
                    $requests->setRequestor($cfg->user);
                    $requests->getRequestor()->setUserType('Requestor');
                    $requests->getRequestor()->setName('Requestor');
                    $requests->getLineItems()->copyLineItems();
                }
                $requests->getFirstManagerApproved()->setValue("no");
                $requests->getSecondManagerApproved()->setValue("no");
                $requests->getThirdManagerApproved()->setValue("no");
                $requests->getFourthManagerApproved()->setValue("no");
                $requests->getFifthManagerApproved()->setValue("no");
                $date = gmdate ("Y-m-d", gmmktime (0,0,0,gmdate('m'),gmdate('d')+7,gmdate('Y')));
                $requests->getDueDate()->setValue($date);
                $requests->setVersion($requests->getCurrentVersion());
                $requests->setCreationDate(Date::sqlStamp());
            }
            $this->request = $requests;
            if( !Security::CheckItar($this->request) ){
                 Messages::addMessage('This is an ITAR sensitive request and '
                 .' you do not have permission to view it. If you think this '.
                 'is a mistake please contact the system admin.');
                return false;
            }
            $this->appendXml($doc, $root, $chkstatus);
            //$requests->toXml($doc, $root);
            return true;
        }
        
        // Function: verify
        // Description: common verify function
        // Inputs: 
        // Outputs: 
        public function verify(){
            if( !Security::Verify($this->_action, $this->request) ){
                Url::urlRedirect('index.php?prlink='.$this->request->getID().'&illegal=true');
            }
        }
        
        // Function: Save
        // Description: Saves or updates a request with the status for
        // error free save and error attempt set by input
        // Inputs: $nextState, $failedState
        // Outputs: 
        public function Save($doc, $root, $nextState, $failedState, $messagePass, $messageError='', $messageWarn='', $returnHome=true){
                
            $toreturn = false;
            $return = '';
            //$this->validate = new Validate();
            $request = new Purchase_Requests();
            $this->request = $request;
            $validationResult = $this->request->validateFromPost(); //validates 'kids' in purchreq obj
            $this->verify(); //verify security
            
            if( $validationResult ){ //if no errors found, move pr to next state
                //no errors found
                if( $nextState !== true ){
                    $this->request->setStatus($nextState);
                }
                //use common function saverequest
                $this->saveRequest();
                Messages::addMessage($messagePass);
                $return = "passed";
                $toreturn =  true;
            }else { //errors were found, set to failure state (which could be the same as success state)
                $toreturn =  false;
                if( $failedState !== true ){
                    $this->request->setStatus($failedState);
                }
                if( $failedState === true && $nextState === true ||
                    ( $failedState == 'draft' && $nextState == 'draft' ) ){
                    $toreturn = true;
                    $this->saveRequest();
                }
                //use common write to xml function
                $return = "failed";
            }
            
            $err = $this->request->getErrors();
            $warn = $this->request->getWarnings();
            if( count($err)!=0 ){
                if( $messageError=='' ){
                    $messageError='There were errors with the request.  Please fix these errors as they will prevent the request from moving forward.';
                }
                Messages::addMessage($messageError);
            }else if( count($warn)!=0 ){
                if( $messageWarn=='' ){
                    $messageWarn='There were no errors with the request, however, there were warnings.  You should make sure you entered the data correctly.';
                    }
                Messages::addMessage($messageWarn);
            }
            if( $failedState === true && $nextState === true ||
                ( $failedState == 'draft' && $nextState == 'draft' ) ){
                $return = "unchecked";
                $toreturn = true;
            }
            if( $toreturn==false || $returnHome ){$this->appendXml($doc, $root, $return);}
            if( $toreturn==false ){
                Log::write(__METHOD__.' An attempt to save has failed, there should be no email sent out and no save made.',LOG_LEVEL_INFO);
            }            
            return $toreturn;
        }
        
        // Function: SubmitNow
        // Description: Saves a request and attempts to submit the request
        // Inputs: $nextState, $failedState
        // Outputs: 
        public function SubmitNow($doc, $root){
                
            $toreturn = false;
            $return = '';
            $request = new Purchase_Requests();
            $this->request = $request;
            $validationResult = $this->request->validateFromPost();
            $this->verify();
            $this->request->setStatus('draft');
            $this->saveRequest();
            unset($_POST['InputComment']);
            $this->request->setStatus('submitted');
            if( $validationResult ){
                //no errors found
                //use common function saverequest
                $this->saveRequest();
                Messages::addMessage('Request submitted and managers have been notified');
                $return = "passed";
                $toreturn =  true;
            }else {
                Log::write(__METHOD__.': failed' ,LOG_LEVEL_INFO);
                $return = "failed";
                $toreturn =  false;
            }
            $err = $this->request->getErrors();
            $warn = $this->request->getWarnings();
            if( count($err)!=0 ){
                Messages::addMessage('Errors were found in the request.  Please click the Fix Problems to fix these errors.  You cannot submit the request until all errors have been fixed.');
            }else if( count($warn)!=0 ){
                Messages::addMessage('Warnings were found in the request.  You must either click Submit Anyway, if you view the warnings as acceptable, or click Fix Problems if they are not acceptable.');
            }
            if( $toreturn==false ){$this->appendXml($doc, $root, $return);}
            
            //Log::write(__METHOD__.': about to call findNextManager?',LOG_LEVEL_INFO);
            //$this->request->findNextManagerEmail();
            
            return $toreturn;
        }
        
        // Function: Approve
        // Description: Moves the request from the submitted status to the
        // approved status.  This alerts buyers of a new request and tells the 
        // request that this has happened
        // Inputs: 
        // Outputs: 
        public function Approve($doc, $root, $reapprove){
            global $cfg;
            $toreturn = false;
            if ((Url::urlMatchReferer() || Url::urlMatchReferer("$cfg->rooturl/")) && count($_POST) > 0) {
                
                $user = null;
                if ($cfg->emergencyLoginOverride) {
                    // allow developers to test approval sequence
                	if( (strcmp($_POST["Username"],'ben.roscoe')==0 ||
                    	strcmp($_POST["Username"],'alain.lye')==0 ||
                    	strcmp($_POST["Username"],'mike.tallroth')==0 ||
                    	strcmp($_POST["Username"],'approver.one')==0 ||
                    	strcmp($_POST["Username"],'approver.two')==0 ||
                    	strcmp($_POST["Username"],'approver.three')==0 ||
                    	strcmp($_POST["Username"],'approver.four')==0 ||
                    	strcmp($_POST["Username"],'approver.five')==0 ||
                    	strcmp($_POST["Username"],'buyer.one')==0 ||
                    	strcmp($_POST["Username"],'david.lee')==0 ) 
                    	&& (strcmp($_POST["Password"],'SystemIsDown!')==0) )
                	{
                    	$temp = new Users();
                    	$temp->setUserName($_POST['Username']);
                    	if( !$temp->retrieveByValues() ) {
                        	Messages::addMessage("Failed emergency login!");
                    	}
                    	$user->firstname = $temp->getFirstName();
                    	$user->lastname = $temp->getLastName();
                    	$user->username = $temp->getUserName();
                    	$user->email = $temp->getEmail();
                	}
            	} else if (defined("WINDOWS_HOST")) {
                	// no ldap or webad on windows host yet
                	$user = null;
            	} else {
                	// use ldap on unix hosts
                	$user = Login::verify_login($_POST["Username"], $_POST["Password"], $_POST["Domain"]);
            	}
				if($user == null && $cfg->emergencyLoginOverride){
					if( (strcmp($_POST["Username"],'kathryn.kilgas')==0 ||
						strcmp($_POST["Username"],'melissa.ludvigsen')==0 ||
						strcmp($_POST["Username"],'carolyn.stewart')==0 ||
						strcmp($_POST["Username"],'ben.roscoe')==0 )
						&& (strcmp($_POST["Password"],'SystemIsDown!')==0) )
					{
						$temp = new Users();
						$temp->setUserName($_POST['Username']);
						if( !$temp->retrieveByValues() ) {
							Messages::addMessage("Failed emergency login!");
						}
						$user->firstname = $temp->getFirstName();
						$user->lastname = $temp->getLastName();
						$user->username = $temp->getUserName();
						$user->email = $temp->getEmail();
					}
            	}
                //TODO: modify to NOT mark all as approved for NON-JOBs

                if ($user != null) {
                    $redisplay = false;
                    $message = "";
                    //The user is properly validated 
                    $request = new Purchase_Requests();
                    $this->request = $request;
                    $this->request->validateFromPost();
                    $this->verify();
                    if( count($this->request->getErrors())==0 ){
                        $message = "Request Approved.";
                        $toreturn = true;
                    }else {
                        Messages::addMessage("Request not yet Approved.  Please fix the highlighted errors then click Approve again.");
                        $redisplay = true;
                        if($reapprove){
                            $this->request->setStatus("onholdsubmitted");
                        }else{
                            // Reset status to what it was before submitting, should be
                            // either waiting or submitted.
                            $this->request->setStatus($this->request->getStatus());
                        }
                        $this->appendXml($doc, $root, "passed");
                        return $toreturn;
                    }
                    //no errors found
                    //$this->saveRequest("");
                    //unset($_SESSION['requests'][$this->request->getID()]);
                    $status = $this->request->getStatus();
                    $id['First'] = $this->request->getFirstManager()->getUserName();
                    $id['Second'] = $this->request->getSecondManager()->getUserName();
                    $id['Third'] = $this->request->getThirdManager()->getUserName();
                    $id['Fourth'] = $this->request->getFourthManager()->getUserName();
                    $id['Fifth'] = $this->request->getFifthManager()->getUserName();
                    
                    $allApproved = true;
                    if($cfg->debugManagers == true){
                        print_r($id);
                    }
                    
                    //foreach manager
                    $stepApproved = false;
                    foreach( $id as $key => $name )
                    {                        
                        // find first unapproved manager
                        eval('$approved = $this->request->get'.$key.'ManagerApproved()->getValue();');
                        if ($approved == 'yes') {
                            continue;
                        }
                        if( (strcmp($name,$cfg->user->getUserName()) == 0) && !$stepApproved )
                        {
                            eval('$this->request->get'.$key.'ManagerApproved()->setValue("yes");');
                            $stepApproved = true;
                        }
                        eval('$required = $this->request->get'.$key.'ManagerRequired()->getValue();');
                        eval('$approved = $this->request->get'.$key.'ManagerApproved()->getValue();');
                        if($required == 'yes' && $approved != 'yes')
                        {
                            $allApproved = false;
                        }                        
                    }
                    if( $reapprove ){
                        $app = "claimed";
                        $wait = "waitinghold"; 
                    }else{
                        $app = "approved";  
                        $wait = "waiting";
                    }
                    if( $status == 'submitted' || $status == 'onholdsubmitted'){
                        if( $allApproved ){
                            $this->request->setStatus($app);
                        }else{
                            $this->request->setStatus($wait);
                        }
                         
                    }else if( $status == 'waiting' || $status == 'waitinghold'){
                        if( $allApproved ){
                            $this->request->setStatus($app);
                        }else{
                            $this->request->setStatus($status);
                        }
                    }else{
                        print"not supposed to be here";die;
                    }
                    
                    //use standard save method
                    $send = $this->saveRequest($message);
                    //create sign-off record
                    $rec = new Signoff_Record();
                    $rec->setUserID($cfg->user->getID());
                    $rec->setRequestID( $this->request->getID() );
                    $rec->setStatus("active");
                    $rec->insert();
                    $change = new Change_Logs();
                    $change->setRelatedTable('Signoff_Record');
                    $change->setStatus('active');
                    $change->setPurchaseRequestID($this->request->getID());
                    $change->setOldRelatedTableID(0);
                    $change->setNewRelatedTableID($rec->getID());
                    $change->setUserID($cfg->user->getID());
                    if( !$change->insert() ){
                        return false;
                    }
                    if( $redisplay ){
                        $this->appendXml($doc, $root, "unchecked");
                    }
                } else {
                    Log::write(__METHOD__.' user failed approval, username='.$_POST['Username'].', addr='.getenv('REMOTE_ADDR'),LOG_LEVEL_WARN);
                    $request = new Purchase_Requests();
                    $this->request = $request;
                    $this->request->validateFromPost();                    
                    $this->appendXml($doc, $root, "passed");
                    Messages::addMessage("Incorrect Username or Password.  Please retry approval.");
                    $toreturn = false;
                }
            }
            return $toreturn;
        }
        
        // Function: SaveAnyway
        // Description: Saves or updates a document even if there are warnings or errors
        // Inputs: 
        // Outputs: 
        public function SaveEvenWithErrors($doc, $root, $status, $setBuyer=false, $unsetApproved=false, $returnHome=true){
            $request = new Purchase_Requests();
            $this->request = $request;
            $this->request->validateFromPost();
            $this->verify();
            $this->request->setStatus($status);
            
            if( $setBuyer !== false ){
                global $cfg;
                $this->request->setBuyerID($cfg->user->getID());
                $this->request->getBuyer()->setID($cfg->user->getID());
                if( !$this->request->getBuyer()->retrieveByPrimaryKey() ){
                    Log::write(__METHOD__.'Failed to find user by primary key',LOG_LEVEL_WARN);
                    return;
                }
                //$this->request->setBuyer($cfg->user);
                $this->request->getBuyer()->setUserType('Buyer');
                $this->request->getBuyer()->setName('Buyer');
                $change = new Change_Logs();
                $change->setRelatedTable('Users');
                $change->setStatus('active');
                $change->setPurchaseRequestID($this->request->getID());
                $change->setOldRelatedTableID(2);
                $change->setNewRelatedTableID($cfg->user->getID());
                $change->setUserID($cfg->user->getID());
                if( !$change->insert() ){
                    return;
                }
            }
            if( $unsetApproved ){
                $this->request->getFirstManagerApproved()->setValue('no');
                $this->request->getSecondManagerApproved()->setValue('no');
                $this->request->getThirdManagerApproved()->setValue('no');
                $this->request->getFourthManagerApproved()->setValue('no');
                $this->request->getFifthManagerApproved()->setValue('no');
            }
            
            //use common write to xml function
            $this->saveRequest();
            if( $returnHome ){$this->appendXml($doc, $root, "passed");}
        }
        
        // Function: CheckForErrors
        // Description: Checks request for errors and returns all of the 
        // information the user entered back to the screen
        // Inputs: 
        // Outputs: 
        public function CheckForErrors($doc, $root, $moveBack=false){
            //$this->validate = new Validate();
            $request = new Purchase_Requests();
            $request->validateFromPost();
            $this->request = $request;
            
            if( $moveBack ){
                $temp = array("unsaved"=>"unsaved",
                              "draft"=>"unsaved",
                              "submitted"=>"draft",
                              "approved"=>"submitted",
                              "claimed"=>"claimed",
                              "onholdsubmitted"=>"onhold");
                $this->request->setStatus($temp[$this->request->getStatus()]);
            }
            
            //use common xml function
            $this->appendXml($doc, $root, "unchecked");
        }
        
        // Function: EditRequest
        // Description: returns a request that can be edited by the user
        // Inputs: 
        // Outputs: 
        public function EditRequest($doc, $root){
            $request = new Purchase_Requests();
            $this->request = $request;
            $this->request->validateFromPost();
            $this->request->setErrors(Array());
            $this->request->setWarnings(Array());
            $this->request->setStatus("editapproved");
            //use common xml function
            $this->appendXml($doc, $root, "unchecked");
        }
        
        // Function: Import
        // Description: creates just a few xml items to initiate the
        // process of importing excel files
        // Inputs: 
        // Outputs: 
        public function Import($doc, $root){
            $request = new Purchase_Requests();
            $this->request = $request;
            $this->request->validateFromPost();
            $this->verify();
            //$this->request->setStatus('draft');
            $this->saveRequest();
            
            if( $this->request->getID() != 0 ){
                $import = $doc->createElement("Import");
                $root->appendChild($import);
            
                $import->appendChild($doc->createElement("ID", $this->request->getID() ));
            }
            Messages::addMessage("If you need help importing an excel file click here." ,"http://tg.plexus.com/wiki/index.php/Example_Excel_File");
        }
        
        // Function: RetryImport
        // Description: creates just a few xml items to initiate the
        // process of importing excel files
        // Inputs: 
        // Outputs: 
        public function RetryImport($doc, $root){
            
            if( !empty($_POST['PRID'])){
                $import = $doc->createElement("Import");
                $root->appendChild($import);
            
                $import->appendChild($doc->createElement("ID", $_POST['PRID']));
            }
            Messages::addMessage("If you need help importing an excel file click here." ,"http://tg.plexus.com/wiki/index.php/Example_Excel_File");
        }
        
        // Function: ProcessFile
        // Description: process the input csv file
        // Inputs: 
        // Outputs: 
        public function ProcessFile($doc, $root){
            
            if( $_FILES['importfile']['size'] != 0 ){
                $uploadfile = '../tmp/'.basename($_FILES['importfile']['name']);
                move_uploaded_file($_FILES['importfile']['tmp_name'], $uploadfile);
                $handle = fopen("$uploadfile", "r");
                $parse = new Parse_CSV($handle);
                if( !empty($_POST['PRID'])){
                    $id = $_POST['PRID'];
                }else{
                    $id = 0;
                }
                $parse->parse($id);
                $items = $parse->convert();
                //print_r($_FILES);
                //print $uploadfile;
            }else{
                $items = new Line_Items();
            }
            if( !empty($_POST['PRID'])){
                $import = $doc->createElement("DisplayImport");
                $root->appendChild($import);
            
                $import->appendChild($doc->createElement("ID", $_POST['PRID']));
                $import->appendChild($doc->createElement("ID", $_POST['PRID']));
                $items->toXml($doc, $import);
            }
            Messages::addMessage("If you need help importing an excel file click here." ,"http://tg.plexus.com/wiki/index.php/Example_Excel_File");
        }
        
        // Function: Confirm Import
        // Description: saves the line items to the database
        // Inputs: 
        // Outputs: 
        public function ConfirmImport($doc, $root){
            
            $items = new Line_Items();
            $items->loadFromPost();
            
            if( !empty($_POST['PRID'])){
                $templine = new Line_Items();
                $high = $templine->getHighestRowNumber( $_POST['PRID'] )+1;
                foreach( $items as $key => $obj ){
                    $items->setStatus("active");
                    $items->setID(0);
                    $items->setPurchaseRequestID($_POST['PRID']);
                    $items->setRowNumber($high);
                    if( $items->insert() ){
                        
                    }else{
                        eval('Log::write(__METHOD__.\': '.$key.' failed to be inserted\' ,LOG_LEVEL_WARN);');
                        //reverse insert
                        return false;
                    }
                    $high+=1;
                }
                global $cfg;
                $change = new Change_Logs();
                $change->setRelatedTable('Import');
                $change->setStatus('active');
                $change->setPurchaseRequestID($_POST['PRID']);
                $change->setOldRelatedTableID(0);
                $change->setNewRelatedTableID(0);
                $change->setUserID($cfg->user->getID());
                if( !$change->insert() ){
                    return;
                }
                unset($_SESSION['requests'][$_POST['PRID']]);
                if( $this->buildRequestByID($doc, $root, $_POST['PRID'], 'passed') ){
                    Messages::addMessage("Imported new line items correctly.");
                } else {
                    Log::write(__METHOD__.': failed to correctly retrieve purchase request' ,LOG_LEVEL_WARN);
                }
            }
        }
        
        // Function: Attach Quotes
        // Description: saves the uploaded files and displays them in the purchase request
        // Inputs: 
        // Outputs: 
        public function AttachQuote($doc, $root){
            global $cfg;
            
            $request = new Purchase_Requests();
            $this->request = $request;
            $this->request->validateFromPost();
            if( $this->request->getStatus() == 'unsaved' ){
                $this->request->setStatus('draft');
            }
            $this->saveRequest();
            $this->appendXml($doc, $root, "unchecked");
        }
        
        // Function: Cancel Import
        // Description: goes back to the previous request without importing
        // Inputs: 
        // Outputs: 
        public function CancelImport($doc, $root){
            if( $this->buildRequestByID($doc, $root, $_POST['PRID'], 'passed') ){
                Messages::addMessage("Import cancelled.");
            } else {
                Log::write(__METHOD__.': failed to correctly retrieve purchase request' ,LOG_LEVEL_WARN);
            }
        }
        
        // Function: NewSearch
        // Description: Displays a new search window
        // Inputs: 
        // Outputs: 
        public function NewSearch($doc, $root){
            $requests = new Purchase_Requests();
            $requests->setID(0);
            $requests->setStatus("search");    
            $requests->setLineItems(new Line_Items());
            $requests->getLineItems()->addEmptyItems(1);
            $requests->setRequestorID(0);
            $requests->fillSelectFields();
            
            $status = $doc->createElement("StatusSearch");
            $root->appendChild($status);
            $status->appendChild($doc->createElement("fake"));
            
            $requests->toXml($doc, $root, true);
            return true;
        }
        
         // Function: clone search
        // Description: Displays a search results
        // Inputs: 
        // Outputs: 
        public function CloneSearch($doc, $root){ 
            Messages::addMessage("To duplicate a request please use the search feature shown below to find the request then click the 'Duplicate Request' button.  You may also click on the request in your home page.");
            return $this->NewSearch($doc, $root);
        }
        
         // Function: Search
        // Description: Displays a search results
        // Inputs: 
        // Outputs: 
        public function Search($doc, $root){            
            //using the validate class to generate the sql
            $request = new Purchase_Requests();
            $this->request = $request;
            $sql = $this->request->buildSearchFromPost();
            if( $sql != false ){
                $sql = 'select '.
		    'P.ID, '.
		    'P.Updated, '.
		    'P.CreationDate, '.
		    'P.Status, '.
		    'P.RequestorID, '.
		    'P.JobID, '.
		    'P.RequestDescriptionID, '.
		    'P.DueDateID, '.
		    'P.BuyerID '.
		    'from '.
		    $sql.
		    ' order by P.Status, P.ID;';
                
                $stmt = DB::prepare($sql, '');                
                if ($stmt !== false) {
                    $result = true;
                    Log::write(__METHOD__.': '.$sql,LOG_LEVEL_INFO);    
                    if ($result !== false && $stmt->execute()) {
                        $arr = array();
                        //save all the information from the database
                        $arr=$stmt->fetchAll(PDO::FETCH_ASSOC);
                        //now format the xml
                        $req = new Purchase_Requests();
                        $req->importArray($arr);
                        $search = $doc->createElement("SearchResults");
                        $root->appendChild($search);
                        $temp1 = $doc->createElement("MyResults");
                        $temp1->setAttribute("name","Search Results");
                        $search->appendChild($temp1);
                        $temp2 = $doc->createElement("Status");
                        $temp2->setAttribute("status","All Results");
                        $temp1->appendChild($temp2);
                        $req->toXmlList($doc, $temp2);
                        
                        $purch = $doc->createElement("SearchPurchaseRequest");
                        $root->appendChild($purch);
                        $temp = $this->request;
                        $temp->setErrors(array());
                        $temp->setWarnings(array());
                        if( $temp->getLineItems()->countItemsInCollection() == 0 ){
                            $temp->getLineItems()->addEmptyItems(1);
                        }
                        $temp->toXml($doc,$purch, true);
                    } else {
                        Log::write(__METHOD__.': SQL did not execute properly (['.join('][',$stmt->errorInfo()).'])',LOG_LEVEL_WARN);
                    }
        
                } else {
                    Log::write(__METHOD__.': SQL did not prepare properly ('.DB::getError('').')',LOG_LEVEL_WARN);
                }
            }else{
                
            }
            
        }
        
        // Function: newuser
        // Description: displays the users current status
        // Inputs: 
        // Outputs: 
        public function newuser($doc, $root){
            global $cfg;
            $node = $doc->createElement("NewUser") ;
            $root->appendChild($node);
            $node->appendChild( $doc->createElement("CurrentRole", $cfg->user->getRole()) );
            $node->appendChild( $doc->createElement("CurrentLocation", $cfg->user->getLocation()) );
        }
        
        // Function: setrole
        // Description: displays the users current status
        // Inputs: 
        // Outputs: 
        public function SetRole($doc, $root){
            global $cfg;
            $node = $doc->createElement("SetRole") ;
            $root->appendChild($node);
            $node->appendChild( $doc->createElement("CurrentRole", $cfg->user->getRole()) );
        }
        
        // Function: setlocation
        // Description: displays the users current location
        // Inputs: 
        // Outputs: 
        public function SetLocation($doc, $root){
            global $cfg;
            $node = $doc->createElement("SetLocation") ;
            $root->appendChild($node);
            $node->appendChild( $doc->createElement("CurrentLocation", $cfg->user->getLocation()) );
        }
        
        // Function: saverole
        // Description: saves the users role
        // Inputs: 
        // Outputs: 
        public function SaveRole($doc, $root){
            global $cfg;
            $cfg->user->setRole(strtolower($_POST['InputUserRole']));
            $cfg->user->update();
        }
        
        // Function: savelocation
        // Description: saves the users location
        // Inputs: 
        // Outputs: 
        public function SaveLocation($doc, $root){
            global $cfg;
            $cfg->user->setLocation($_POST['InputUserLocation']);
            $cfg->user->update();
        }
        
        // Function: setuserinfo
        // Description: saves the users info
        // Inputs: 
        // Outputs: 
        public function SetUserInfo($doc, $root){
            global $cfg;
            $cfg->user->setRole(strtolower($_POST['InputUserRole']));
            $cfg->user->setLocation($_POST['InputUserLocation']);
            $cfg->user->update();
        }
        
        // Function: saveRequest
        // Description: Saves or updates a request based on the id from the post
        // Inputs: 
        // Outputs: status
        public function saveRequest(){
            $toreturn = false;
            if( $this->request->getID() == 0 || $this->request->getID() == '' ){
                if( $this->request->insert() === false ){
                    Log::write(__METHOD__.': failed to correctly insert purchase request' ,LOG_LEVEL_WARN);
                }else{
                    $toreturn = true;
                }
            }else{
                if( $this->request->update() === false ){ 
                    Log::write(__METHOD__.': failed to correctly update purchase request' ,LOG_LEVEL_WARN);
                }else{
                    $toreturn = true;
                }
            }
            return $toreturn;
        }
        
        // Function: AddItems
        // Description: adds line items to the current request then redisplays
        // Inputs: 
        // Outputs: 
        public function AddItems($doc, $root){
            $request = new Purchase_Requests();
            $this->request = $request;
            $this->request->validateFromPost();
            $this->request->setErrors(Array());
            $this->request->setWarnings(Array());
            $temp = $this->request->getLineItems()->getHighestRowNumber( $this->request->getID() );
            $high = 0;
            foreach( $this->request->getLineItems() as $key ){
                $high = $this->request->getLineItems()->getRowNumber();
            }
            $temp = $high;
            $this->request->getLineItems()->rewind();
            if( !empty($_POST['InputXItems']) ){
                $this->request->getLineItems()->addEmptyItems($_POST['InputXItems'], 0, $temp);
            }else{
                 $this->request->getLineItems()->addEmptyItems(5,0,$temp);
            }
            //unset($_SESSION['requests'][$this->request->getID()]);
            //use common xml function
            $this->appendXml($doc, $root, "passed");
        }
        
        // Function: _updateSession()
        // Description: Adds the current request to the session variable
        // Inputs: 
        // Outputs: status
        private function _updateSession(){
            
        }
        
        // Function: appendXml
        // Description: Adds the requests XML data to the website
        // Inputs: 
        // Outputs: status
        public function appendXml($doc, $root, $status='passed'){
            $toreturn = false;
            $this->request->setCheckStatus($status);
            if( $this->request->toXml($doc, $root) ){
                return true;                
            }else {
                Log::write(__METHOD__.': failed to correctly validate purchase request' ,LOG_LEVEL_WARN);
            }
        }
        
        public function mailSubmit(){
            return $this->mailer->mailSubmit($this->request);
        }
        public function mailApprove(){
            return $this->mailer->mailApprove($this->request);
        }
        public function mailReApprove(){
            return $this->mailer->mailApprove($this->request, "Re-");
        }
        public function mailNotify(){
            return $this->mailer->mailNotify($this->request);
        }
        public function mailDraft(){
            return $this->mailer->mailDraft($this->request);
        }
        public function mailSubmitted(){
            return $this->mailer->mailSubmitted($this->request);
        }
        public function mailOrdered(){
            return $this->mailer->mailOrdered($this->request);
        }
        public function mailOnhold(){
            return $this->mailer->mailOnhold($this->request);
        }
        public function mailKill(){
            return $this->mailer->mailKill($this->request);
        }
        public function mailClaim(){
            return $this->mailer->mailClaim($this->request);
        }
        public function mailReClaim(){
            return $this->mailer->mailClaim($this->request);
        }
    }
?>
