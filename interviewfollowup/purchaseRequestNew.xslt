<xsl:transform version="1.1" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="inputStyle.xslt"/>
<xsl:import href="buttondisplay.xslt"/>
<xsl:import href="changes.xslt"/>
<xsl:import href="columns.xslt"/>
<xsl:import href="attachments.xslt"/>

<xsl:template name="PurchaseRequest">
<xsl:param name="node" /> 

        <form method="post" enctype="multipart/form-data" id="PurchaseRequestForm" onsubmit="doSubmit()" 
              action="{concat('index.php?prlink=',/PurchaseWebsite/PurchaseRequest/ID)}">
        <xsl:choose>
        <xsl:when test="/PurchaseWebsite/StatusSearch">
            <h2 class="purchheader">Please enter your search terms</h2>
        </xsl:when>
        <xsl:when test="/PurchaseWebsite/SearchResults">
            <h2 class="purchheader">Results of your search are listed below</h2>
        </xsl:when>
        <xsl:otherwise>
            <h2 class="purchheader">Purchase Request: PR<xsl:value-of select="$node/ID"/> Status: <xsl:value-of select="$node/Status"/></h2>
        </xsl:otherwise>
        </xsl:choose>
        <xsl:call-template name="ButtonDisplay">
            <xsl:with-param name="node" select="$node"/>
            <xsl:with-param name="top" select="'true'"/>
        </xsl:call-template>
        <br clear="both"/>
        <!--this section displays a purchase request header, each template call adds another box-->
        <div id="RequestHeader" class="Box" >
            <div class="header">Header Information Section<img src="images/arrow.gif" id="arrow" onclick="toggleVis('headerbody')"/></div>
            <div class="body" id="headerbody" style="display:block">
                <xsl:apply-templates select="$node/Errors"/>
                <xsl:apply-templates select="$node/Warnings"/>
                <div class="Line">
                    <xsl:call-template name="Line1Col1">
                        <xsl:with-param name="node" select="$node"/>
                    </xsl:call-template>
                    <xsl:call-template name="Line1Col2">
                        <xsl:with-param name="node" select="$node"/>
                    </xsl:call-template>
                    <xsl:call-template name="Line1Col3">
                        <xsl:with-param name="node" select="$node"/>
                    </xsl:call-template>
                </div>
                <br clear="both"/>
                <div class="Line">
                    <xsl:call-template name="Line2Col1">
                        <xsl:with-param name="node" select="$node"/>
                    </xsl:call-template>
                    <xsl:call-template name="Line2Col2">
                        <xsl:with-param name="node" select="$node"/>
                    </xsl:call-template>
                    <xsl:call-template name="Line2Col3">
                        <xsl:with-param name="node" select="$node"/>
                    </xsl:call-template>
                </div>
                <br clear="both"/>
                <div class="Line">
                    <xsl:call-template name="Line3Col1">
                        <xsl:with-param name="node" select="$node"/>
                    </xsl:call-template>
                    <xsl:call-template name="Line3Col2">
                        <xsl:with-param name="node" select="$node"/>
                    </xsl:call-template>
                    <xsl:call-template name="Line3Col1Col2">
                        <xsl:with-param name="node" select="$node"/>
                    </xsl:call-template>
                </div>
            </div>
        </div><!--end header-->
        
        <!--this section displays the line items-->
        <br clear="both"/>
        <div id="LineItemsBox" class="Box" >
            <div class="header">Line Items for this Purchase Request<img src="images/arrow.gif" id="arrow" onclick="toggleVis('LineItemsBox2')"/></div>                     
            <div class="body" id="LineItemsBox2">
                <div id="LineNavHead">Navigate cells with Ctrl+(up,down,left,right)
                <input type="button" onclick="doPrint();" value="Print" /> </div>
                <xsl:apply-templates select="$node/LineItems"/>
                <xsl:if test="not(starts-with($node/Status,'search'))">
                    <div id="AddEmptyLines">
                        <table style="width:100%;"> <tr> <td style="text-align:left;">
                            <label for="InputXItems">Add this many blank Line Items: </label>
                            <input type="text" name="InputXItems" id="InputXItems" value="5" size="2"/>
                            <input type="submit" name="action" value="Add Items" /><br clear="both"/>
                            <label for="copylineitems">Click this button to copy specific line items to another request: </label>
                            <input type="button" name="copylineitems" value="Copy" 
                                onclick="{concat('findRequestsToCopy(',/PurchaseWebsite/PurchaseRequest/CurrentUser/Users/Item/ID,')')}"/>
                            <br/><span>Click to export line items to csv file: </span>
                            <input type="button" name="exportlineitems" value="Export"
                                onclick="doExport({concat('',/PurchaseWebsite/PurchaseRequest/ID)})"/>
                        </td> <td style="text-align:right;">
                            <a target="_blank" title="PCB, Electronics &amp; CEC's" href="http://intranet.plexus.com/mfgops/supply_chain_and_materials/sourcing/Commodities/default.aspx">Plexus Matrix Suppliers</a>
                            <br/>
                            <a target="_blank" title="Supplier list for US, Livingston &amp; Penang" href="http://intranet.plexus.com/engsrvcs/Administrative_Services/materials/default.aspx">PLXS-ENG Qualified Supplier List</a>
                        </td> </tr> </table>
                    </div>
                </xsl:if>
            </div>
        </div>
        <br clear="both"/>
        <!--this section displays the attached quotes-->
        <xsl:call-template name="Attachments">
            <xsl:with-param name="node" select="$node"/>
        </xsl:call-template>
        <!--this section displays the changes made to a request-->
        <xsl:apply-templates select="$node/Changes"/>
        <br clear="both"/>
        <xsl:call-template name="ButtonDisplay">
            <xsl:with-param name="node" select="$node"/>
            <xsl:with-param name="top" select="''"/>
        </xsl:call-template>
        
        <!--this section puts info about the purchase request into the form-->
        <input type="hidden" name="PRID">
            <xsl:attribute name="value">
                <xsl:value-of select="$node/ID"/>
            </xsl:attribute>
        </input>
        <input type="hidden" name="status">
            <xsl:attribute name="value">
                <xsl:value-of select="$node/Status"/>
            </xsl:attribute>
        </input>
        <input type="hidden" name="updated">
            <xsl:attribute name="value">
                <xsl:value-of select="$node/Updated"/>
            </xsl:attribute>
        </input>
        <input type="hidden" name="location" id="location">
            <xsl:attribute name="value">
                <xsl:value-of select="/PurchaseWebsite/PurchaseRequest/Header/RequestCurrencys/Items/Item/ID"/>
            </xsl:attribute>
        </input>
            
        </form>
        <br clear="both"/>
        
</xsl:template>

</xsl:transform>
